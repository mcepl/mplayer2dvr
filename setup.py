#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
import kanaly

setup(name='tv_scripts',
      version=kanaly.__version__,
      description='Scripts making DVR out of mplayer',
      author='Matěj Cepl',
      author_email='mcepl@cepl.eu',
      url='https://gitlab.com/mcepl/mplayer2dvr',
      py_modules=['kanaly'],
      scripts=['cron-record-tv', 'record-tv', 'tv']
      )
